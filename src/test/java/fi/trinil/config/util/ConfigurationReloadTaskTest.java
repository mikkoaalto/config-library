package fi.trinil.config.util;

import org.jmock.Expectations;
import org.junit.runner.RunWith;

import jdave.Block;
import jdave.Specification;
import jdave.junit4.JDaveRunner;

@RunWith(JDaveRunner.class)
public class ConfigurationReloadTaskTest extends Specification<ConfigurationReloadTask> {

	public class WhenReloadingConfiguration {

		
		private ConfigurationRepository service;

		public ConfigurationReloadTask create() {
			service = mock(ConfigurationRepository.class);
			return new ConfigurationReloadTask(service);
		}
		
		public void thenRefreshIsCalled() {
			
			checking(new Expectations() {{
				one(service).refresh();
			}});
			
			context.run();
		}
		
		public void thenRefreshThrowsException() {
			checking(new Expectations() {{
				one(service).refresh();
				will(throwException(new RuntimeException()));
			}});
			
			specify(new Block() {
				public void run() throws Throwable {
					context.run();
				}
			}, should.raise(RuntimeException.class));
			
		}
	}

}
