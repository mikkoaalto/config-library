package fi.trinil.config.util;

import java.util.Timer;

import org.jmock.Expectations;
import org.junit.runner.RunWith;

import fi.trinil.config.ConfigurationFactory;
import fi.trinil.config.DefaultConfiguration;
import jdave.Specification;
import jdave.junit4.JDaveRunner;

@RunWith(JDaveRunner.class)
public class ConfigurationFactoryTest extends Specification<ConfigurationFactory> {

	public class WhenAutoRefreshIsActivated {

		Timer mockTimer = mock(Timer.class);
		
		public ConfigurationFactory create() {
			ConfigurationFactory context = new ConfigurationFactory();
			return context;
		}
		
		public void timerTaskIsCreated() {

			System.setProperty(ConfigurationFactory.CONFIG_RELOAD_DELAY, "100");
			context.setTimer(mockTimer);
			
			checking(new Expectations() {{
				one(mockTimer).scheduleAtFixedRate(with(any(ConfigurationReloadTask.class)), with(equal(100l)), with(equal(100l)));
			}});

			context.setupAutoRefresh();
		}
		
		public void thenTimerTaskCancelled() {

			System.setProperty(ConfigurationFactory.CONFIG_RELOAD_DELAY, "100");
			context.setTimer(mockTimer);
			
			checking(new Expectations() {{
				one(mockTimer).scheduleAtFixedRate(with(any(ConfigurationReloadTask.class)), with(equal(100l)), with(equal(100l)));
				one(mockTimer).cancel();
			}});

			context.setupAutoRefresh();
			context.stopAutoRefresh();
		}
	}
	
	public class WhenAutoReloadIsNotActivated {

		Timer mockTimer = mock(Timer.class);
		
		public ConfigurationFactory create() {
			ConfigurationFactory context = new ConfigurationFactory();
			return context;
		}
		
		public void delayIsNotNumber() {

			System.setProperty(ConfigurationFactory.CONFIG_RELOAD_DELAY, "a");
			context.setTimer(mockTimer);
			
			checking(new Expectations() {{
			}});

			context.setupAutoRefresh();
		}
		
		public void delayIsNull() {

			System.clearProperty(ConfigurationFactory.CONFIG_RELOAD_DELAY);
			context.setTimer(mockTimer);
			
			checking(new Expectations() {{
			}});

			context.setupAutoRefresh();
		}
	}
	
	public class WhenTryingToCreateConfiguration {

		public ConfigurationFactory create() {
			return new ConfigurationFactory();
		}
		
		public void theConfigurationIsCreated() {

			OwnConfig config = (OwnConfig)context.createConfiguration(OwnConfig.class);
			specify(config != null);
		}
	}
	
	private interface OwnConfig extends DefaultConfiguration {
		
	}
}
