package fi.trinil.config.util;

import org.jmock.Expectations;
import org.junit.runner.RunWith;
import org.springframework.web.client.RestTemplate;

import fi.trinil.config.domain.Configuration;
import jdave.Block;
import jdave.Specification;
import jdave.junit4.JDaveRunner;

@RunWith(JDaveRunner.class)
public class ConfigurationRepositoryTest extends Specification<ConfigurationRepository> {

	public abstract class BaseConfiguration {

		RestTemplate restTemplateMock = mock(RestTemplate.class);
		Configuration value = new Configuration();

		public ConfigurationRepository create() {
			System.setProperty(ConfigurationRepository.CONFIG_PROCESS, "proc");
			System.setProperty(ConfigurationRepository.CONFIG_ENVIRONMENT, "env");

			ConfigurationRepository repo = new ConfigurationRepository();
			repo.setRestTemplate(restTemplateMock);
			return repo;
		}

		protected void specifyConfiguration() {
			Configuration config = context.getConfig();

			specify(config, value);
		}
	}


	public class WhenSingleServerConfigurationReadedSuccessfully extends BaseConfiguration {

		public void theConfigurationIsReturned() {

			checking(new Expectations() {{
				one(restTemplateMock).getForObject("http://localhost:8888/proc/env", Configuration.class);
				will(returnValue(value));
			}});

			specifyConfiguration();
		}

		public void sameConfigurationIsReturned() {

			checking(new Expectations() {{
				one(restTemplateMock).getForObject("http://localhost:8888/proc/env", Configuration.class);
				will(returnValue(value));
			}});

			specifyConfiguration();
			specifyConfiguration();
		}
	}


	public class WhenMultiServerConfigurationIsUsed extends BaseConfiguration {

		public ConfigurationRepository create() {
			System.setProperty(ConfigurationRepository.CONFIG_SERVER, "localhost:8889;localhost:8888");
			return super.create();
		}

		public void theConfigurationIsReturned() {

			checking(new Expectations() {{
				one(restTemplateMock).getForObject("http://localhost:8889/proc/env", Configuration.class);
				will(throwException(new RuntimeException()));

				one(restTemplateMock).getForObject("http://localhost:8888/proc/env", Configuration.class);
				will(returnValue(value));
			}});

			specifyConfiguration();
		}

		public void thenTheSameConfigurationIsReturned() {

			checking(new Expectations() {{
				one(restTemplateMock).getForObject("http://localhost:8889/proc/env", Configuration.class);
				will(returnValue(null));

				one(restTemplateMock).getForObject("http://localhost:8888/proc/env", Configuration.class);
				will(returnValue(value));
			}});

			specifyConfiguration();
			specifyConfiguration();
		}

		public void thenReadFails() {
			checking(new Expectations() {{
				one(restTemplateMock).getForObject("http://localhost:8889/proc/env", Configuration.class);
				will(throwException(new RuntimeException()));
				one(restTemplateMock).getForObject("http://localhost:8888/proc/env", Configuration.class);
				will(returnValue(null));
			}});

			specify(new Block() {
				public void run() throws Throwable {
					context.getConfig();
				}
			}, should.raise(ConfigurationException.class));
		}
	}

	public class WhenMultiServerConfigurationWithRetry extends BaseConfiguration {

		public ConfigurationRepository create() {
			System.setProperty(ConfigurationRepository.CONFIG_SERVER, "localhost:8889;localhost:8888");
			System.setProperty(ConfigurationRepository.CONFIG_RETRY_COUNT, "1");
			System.setProperty(ConfigurationRepository.CONFIG_RETRY_DELAY, "100");
			return super.create();
		}

		public void theConfigurationIsReturned() {

			checking(new Expectations() {{
				one(restTemplateMock).getForObject("http://localhost:8889/proc/env", Configuration.class);
				will(throwException(new RuntimeException()));

				one(restTemplateMock).getForObject("http://localhost:8888/proc/env", Configuration.class);
				will(throwException(new RuntimeException()));

				one(restTemplateMock).getForObject("http://localhost:8889/proc/env", Configuration.class);
				will(returnValue(value));

			}});

			specifyConfiguration();
		}
	}

	public class WhenRefreshingConfiguration extends BaseConfiguration {

		public ConfigurationRepository create() {
			System.setProperty(ConfigurationRepository.CONFIG_SERVER, "localhost:8888");
			System.setProperty(ConfigurationRepository.CONFIG_RETRY_COUNT, "0");
			return super.create();
		}
		
		
		public void theConfigurationIsRefreshed() {

			checking(new Expectations() {{
				one(restTemplateMock).getForObject("http://localhost:8888/proc/env", Configuration.class);
				will(returnValue(value));
				
				one(restTemplateMock).getForObject("http://localhost:8888/proc/env", Configuration.class);
				will(returnValue(value));
			}});

			specifyConfiguration();
			context.refresh();
			specifyConfiguration();
		}
		
		public void theConfigurationCannotBeRefreshed() {

			checking(new Expectations() {{
				one(restTemplateMock).getForObject("http://localhost:8888/proc/env", Configuration.class);
				will(returnValue(value));
				
				one(restTemplateMock).getForObject("http://localhost:8888/proc/env", Configuration.class);
				will(throwException(new RuntimeException()));
			}});

			specifyConfiguration();
			specify(new Block() {
				public void run() throws Throwable {
					context.refresh();
				}
			}, should.raise(ConfigurationException.class));
			
			specifyConfiguration();
		}
	}
}
