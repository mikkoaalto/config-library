package fi.trinil.config.util;

import java.util.Date;
import java.util.TimerTask;
import java.util.logging.Logger;

public class ConfigurationReloadTask extends TimerTask {

	private final Logger log = Logger.getLogger(ConfigurationReloadTask.class.getName());
	
	private final ConfigurationRepository service;

	public ConfigurationReloadTask(ConfigurationRepository service) {
		this.service = service;
		
	}
	
	@Override
	public void run() {
		doRefresh();
	}

	private void doRefresh() {
		log.info("Executing configuration reload task");
		System.out.println(new Date().toString() + " : Executing configuration reload task");
		try {
			service.refresh();
		} catch (Throwable th) {
			log.severe("Unable execute configuration reload task");
		}
	}

}
