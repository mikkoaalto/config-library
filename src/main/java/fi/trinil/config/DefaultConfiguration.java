package fi.trinil.config;

/**
 * Default configuration operations.
 * 
 * To add more just create stubs here and add implementation to {@link fi.trinil.config.util.ConfigurationProxy}
 * 
 * @author Mikko Aalto
 * @version 0.1
 *
 */
public interface DefaultConfiguration {

	public String getString(String key);
	
	public Integer getInteger(String key);
	
	public String[] getStringArray(String key);
	
	public void refresh();
}
