package fi.trinil.config;

import java.lang.reflect.Proxy;
import java.util.Timer;

import fi.trinil.config.util.ConfigurationProxy;
import fi.trinil.config.util.ConfigurationReloadTask;

public class ConfigurationFactory {
	
	private static final long MINUTE_IN_MS = 1000*60;
	
	private static final String DEFAULT_RELOAD_DELAY = new Long(MINUTE_IN_MS * 5).toString();
	public static final String CONFIG_RELOAD_DELAY = "CONFIG_RELOAD_DELAY";

	private final ConfigurationProxy proxy = new ConfigurationProxy();

	private Timer timer = new Timer();

	public void setupAutoRefresh() {
		if (enableAutoReload())
			timer.scheduleAtFixedRate(new ConfigurationReloadTask(proxy.getConfRepo()), 
					getConfigurationReloadDelay(), getConfigurationReloadDelay());
	}
	
	private Long getConfigurationReloadDelay() {
		return Long.parseLong(System.getProperty(CONFIG_RELOAD_DELAY, DEFAULT_RELOAD_DELAY));
	}

	private boolean enableAutoReload() {
		try {
			Long.parseLong(System.getProperty(CONFIG_RELOAD_DELAY));
		} catch (NumberFormatException ex) {
			return false;
		}

		return true;
	}

	public DefaultConfiguration createConfiguration(Class<? extends DefaultConfiguration> cClass) {
		return (DefaultConfiguration) Proxy.newProxyInstance(this.getClass().getClassLoader(), new Class[] {cClass}, getProxy());
	}

	public ConfigurationProxy getProxy() {
		return proxy;
	}

	public void stopAutoRefresh() {
		timer.cancel();
	}
	
	public void setTimer(Timer timer ) {
		this.timer = timer;
	}
}
