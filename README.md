Configuration Library
========

The purpose of this library is to provide simple interface for spring cloud configuration server if you are not using the spring annotations in client software.

##Initialize

The parametrization is done using environment values either by using -D -argument of by using terminal environment. 

##Oblicatory parameter:

	* CONFIG_ENVIRONMENT = <name of the process>
	* CONFIG_PROCESS = <Environment>

##Optional parameter:

	* CONFIG_SERVER = <host>:<port>;<host>:<port>
	* CONFIG_RETRY_COUNT = number value
	* CONFIG_RETRY_DELAY = delay in ms

##Default Values:
	
	* CONFIG_SERVER = localhost:8888
	* CONFIG_RETRY_COUNT = 0
	* CONFIG_RETRY_DELAY = 5000


##Usage

Below is a snipped how to get configuration.

```
#!java

public interface OwnConfiguration extends DefaultConfiguration {

	// The method name defines the configuration key
	public String getClientName(); // Will get value for parameter client.name
	
	// Or define the parameter name with ConfigurationKey annotation
	// In this case the method name can be anything 
	@ConfigurationKey("client.name")
	public String getOneParameter();
	
}


OwnConfiguration conf = (OwnConfiguration)new ConfigurationFactory().createConfiguration(OwnConfiguration.class);
conf.getConfigName();
conf.getOneParameter();


```

##Example
	Example can be found from [config-library-example](http://bitbucket.com/mikkoaalto/config-library-example)